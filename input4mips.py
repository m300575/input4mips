#!/usr/bin/env python3
"""Download AMIP boundary conditions.

Further links:
    * https://pcmdi.llnl.gov/mips/amip/
    * https://esgf-node.llnl.gov/search/input4mips/
"""
import glob
import requests
import subprocess
from pathlib import Path
from itertools import product


# Get AMIP input files
def download_file(urlpath, chunk_size=2**10):
    with requests.get(urlpath, stream=True) as ret:
        with open(Path(urlpath).name, "wb") as fp:
            for buf in ret.iter_content(chunk_size=chunk_size):
                if buf:
                    fp.write(buf)


input4mips = (
    "https://esgf-data2.llnl.gov/thredds/fileServer/user_pub_work/input4MIPs/CMIP6Plus/CMIP/PCMDI/PCMDI-AMIP-1-1-9/ocean/mon/tosbcs/gn/v20230512/tosbcs_input4MIPs_SSTsAndSeaIce_CMIP_PCMDI-AMIP-1-1-9_gn_187001-202212.nc",
    "https://esgf-data2.llnl.gov/thredds/fileServer/user_pub_work/input4MIPs/CMIP6Plus/CMIP/PCMDI/PCMDI-AMIP-1-1-9/seaIce/mon/siconcbcs/gn/v20230512/siconcbcs_input4MIPs_SSTsAndSeaIce_CMIP_PCMDI-AMIP-1-1-9_gn_187001-202212.nc",
)


for urlpath in input4mips:
    print(f"Download {Path(urlpath).name}", flush=True)
    download_file(urlpath)

# Remap AMIP input files
target_grids = {
    # gridname: (gridid, start, end)
    "R02B04": ("0043", "1978", "2009"),
    # "R02B07": ("0023", "1978", "2009"),
    "R02B08": ("0033", "1978", "2009"),
    "R02B09": ("0015", "1978", "1989"),
    "R02B10": ("0039", "1978", "1982"),
    # "R02B11": ("0037", "1978", "1980"),
}

for ncin, (gridname, (gridid, start, end)) in product(
    map(Path, input4mips), target_grids.items()
):
    print(f"Remap {ncin.name} to {gridname} between {start}-{end}", flush=True)

    gridfile = (f"/appl/local/climatedt/pool/data/ICON/grids/public/mpim/{gridid}/icon_grid_{gridid}_{gridname}_G.nc")
    ncout = f"{ncin.stem}_{gridid}_{gridname}_G.nc".replace("187001-202212", f"{start}01-{end}12")

    subprocess.run(
        [
            # "echo",
            "/projappl/project_465000454/icon/sw/gcc-12.2.0-zen2/cdo-2.2.2/bin/cdo",
            "-k",
            "auto",
            "-P",
            "16",
            "-setattribute,tosbcs@add_offset=273.15",
            f"-remapbic,{gridfile}",
            f"-select,year={start}/{end}",
            ncin.name,
            ncout,
        ]
    )
